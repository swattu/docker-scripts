#!/usr/bin/env bash
SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

cd ${FOLDER}

docker build -t ws-mongo:latest -f Dockerfile .

mkdir logs

docker rm -f ws-mongo
docker run --name=ws-mongo -d \
    -p 27017:27017 \
    -v ${FOLDER}/config.yml:/mongo/config.yml \
    -v ${FOLDER}/data:/mongo/db \
    -v ${FOLDER}/app-config:/data/configdb \
    -v ${FOLDER}/app-datadb:/data/db \
    -v ${FOLDER}/logs:/mongo/logs \
    ws-mongo:latest
