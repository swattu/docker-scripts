#!/usr/bin/env bash
#script to add environment and to start loopback process manager
mongod --bind_ip_all --logpath /mongo/logs/mongo.log --dbpath /mongo/db --config /mongo/config.yml
