#!/usr/bin/env bash
SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

PRESENT=`docker images -a | grep ws-java8`
if [[ -z "$PRESENT" ]]
then
    echo "Java 8 Image not found. Building."
else
  PRESENT=`echo ${PRESENT} | grep -v month | grep -v week`
  if [[ -z "$PRESENT" ]]
  then
    echo "Java 8 Image is outdated. Re-Building."
  else
    echo "Java 8 Image is already built"
    exit 0
  fi
fi

docker build -t ws-java8:latest -f $FOLDER/dockerfile.txt .
