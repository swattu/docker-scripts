#!/usr/bin/env bash

SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

docker build -t ws-drill:latest -f Dockerfile .

docker rm -f ws-drill

mkdir ${FOLDER}/data
chmod -R 777 ${FOLDER}/data

docker run -i --name ws-drill \
    -p 8047:8047 \
    -p 31010:31010 \
    -t ws-drill:latest
