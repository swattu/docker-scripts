#!/usr/bin/env bash

SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

mkdir -p ${FOLDER}/client
chmod 777 -R ${FOLDER}/client

docker rm -f xymon-client
docker run --name xymon-client \
    --link ws-xymon:ws-xymon \
    -e XYMONSERVERS=ws-xymon \
    -v ${FOLDER}/client:/etc/xymon \
    deweysasser/xymon-client

#    -v ${FOLDER}/client/hosts.cfg:/etc/xymon/hosts.cfg \
