#!/usr/bin/env bash

SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

mkdir -p ${FOLDER}/server
chmod 777 -R ${FOLDER}/server

docker rm -f ws-xymon

docker run \
    -p 80:80 \
    -p 1984:1984 \
    -v ${FOLDER}/server:/etc/xymon \
    --name ws-xymon deweysasser/xymon

