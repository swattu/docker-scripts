# Pre-requisites #

- The output of the following command should be 262144

      sysctl vm.max_map_count
      
- If not, add below line in /etc/sysclt.conf for permanent setting

      vm.max_map_count=262144
      
- For temporary setting

      sysctl -w vm.max_map_count=262144
      
# Running #
- Simply execute

      docker-compose up

- Open the browser

      http://localhost:5601
      
      
# Source #

https://www.elastic.co/guide/en/elastic-stack-get-started/current/get-started-docker.html

# Security #
- ./start-elk.sh
- docker exec -it es bin/elasticsearch-setup-passwords auto
- Capture the generated passwords
- Ubdate "elasticsearch.password" as password of kibana user in dataconfkib/kibana.yml
- ./start-elk.sh

