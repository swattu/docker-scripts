#!/usr/bin/env bash
SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

COUNT=`sysctl vm.max_map_count | cut -b20-`

if [[ ${COUNT} -ne "262144" ]]
then
    echo "Execute the following command"
    echo "sudo sysctl -w vm.max_map_count=262144"
    exit 1
fi
cd ${FOLDER}

BASE_FOLDER=${FOLDER}

ADDITIONAL_COMMAND="--bind_ip_all --config /etc/mongod.yml --auth --replSet rs0 --keyFile /code/keyfile --wiredTigerCacheSizeGB 1"

#This will execute only once

openssl rand -base64 755 > ${BASE_FOLDER}/keyfile
chmod 400 ${BASE_FOLDER}/keyfile
chown 999:999 ${BASE_FOLDER}/keyfile

docker rm -f mongo1 mongo2 mongo3

mkdir -p ${FOLDER}/logs01 ${FOLDER}/logs02 ${FOLDER}/logs03
chmod -R 777  ${FOLDER}/logs01  ${FOLDER}/logs02  ${FOLDER}/logs03

docker run --name mongo1 -d \
    -p 27011:27017 \
    -v ${BASE_FOLDER}/mongod.yml:/etc/mongod.yml \
    -v ${BASE_FOLDER}/logs01:/var/log/mongodb/ \
    -v ${BASE_FOLDER}/app01:/etc/mongo \
    -v ${BASE_FOLDER}/conf01:/data/configdb \
    -v ${BASE_FOLDER}/data01:/data/db \
    -v ${BASE_FOLDER}/keyfile:/code/keyfile \
    -v ${BASE_FOLDER}/replica.js:/code/replica.js \
    mongo:3.6.9-stretch ${ADDITIONAL_COMMAND}

docker run --name mongo2 -d \
    -p 27012:27017 \
    -v ${BASE_FOLDER}/mongod.yml:/etc/mongod.yml \
    -v ${BASE_FOLDER}/logs02:/var/log/mongodb/ \
    -v ${BASE_FOLDER}/app02:/etc/mongo \
    -v ${BASE_FOLDER}/conf02:/data/configdb \
    -v ${BASE_FOLDER}/data02:/data/db \
    -v ${BASE_FOLDER}/keyfile:/code/keyfile \
    mongo:3.6.9-stretch ${ADDITIONAL_COMMAND}

docker run --name mongo3 -d \
    -p 27013:27017 \
    -v ${BASE_FOLDER}/mongod.yml:/etc/mongod.yml \
    -v ${BASE_FOLDER}/logs03:/var/log/mongodb/ \
    -v ${BASE_FOLDER}/app03:/etc/mongo \
    -v ${BASE_FOLDER}/conf03:/data/configdb \
    -v ${BASE_FOLDER}/data03:/data/db \
    -v ${BASE_FOLDER}/keyfile:/code/keyfile \
    mongo:3.6.9-stretch ${ADDITIONAL_COMMAND}

echo "Sleeping for 5 seconds"

sleep 5

echo "========================================================"
echo "If this is first time, then execute the following script"
echo "inside the docker(Command Below)."
echo "docker exec -it mongo1 mongo"
echo "========================================================"
cat replica.js
echo "========================================================"
