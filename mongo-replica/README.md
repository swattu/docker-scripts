# Pre-requisites #

- The output of the following command should be 262144

      sysctl vm.max_map_count
      
- If not, add below line in /etc/sysclt.conf for permanent setting

      vm.max_map_count=262144
      
- For temporary setting

      sysctl -w vm.max_map_count=262144
      
# Running #
- Simply execute

      ./start-replica.sh
