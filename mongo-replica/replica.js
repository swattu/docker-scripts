// Change the host to the local IP.
rs.initiate(
    {
        _id : 'rs0',
        members: [
            {_id: 0, host: "host:27011"},
            {_id: 1, host: "host:27012"},
            {_id: 2, host: "host:27013"}
        ]
    }
);

sleep(5000);

rs.slaveOk();

var adminDb = db.getSiblingDB("admin")

adminDb.createUser(
    {
        user: "admin",
        pwd: "admin123",
        roles: ["userAdminAnyDatabase", "readWriteAnyDatabase", "dbAdminAnyDatabase", "root"]
    }
);

adminDb.getUsers();

