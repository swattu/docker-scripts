#!/usr/bin/env bash

SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

docker stop ws-postgres

docker rm ws-postgres

docker run --name ws-postgres -d \
    -p 5432:5432 \
    -e POSTGRES_USER=ws \
    -e POSTGRES_PASSWORD=ws123 \
    -v ${FOLDER}/data:/var/lib/postgresql/data \
    postgres:11-alpine

