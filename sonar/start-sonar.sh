#!/usr/bin/env bash
SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

${FOLDER}/../postgres/start-postgres.sh

COUNT=`sysctl vm.max_map_count | cut -b20-`

if [[ ${COUNT} -ne "262144" ]]
then
    echo "Execute the following command"
    echo "sudo sysctl -w vm.max_map_count=262144"
    exit 1
fi

#In case of any issues run the following commands as sudo in host.
#sudo sysctl -w vm.max_map_count=262144
#sudo sysctl -w fs.file-max=65536
#sudo ulimit -n 65536
#sudo ulimit -u 4096

mkdir -p ${FOLDER}/sonarqube

chmod 777 -R ${FOLDER}/sonarqube

JDBC_URL=jdbc:postgresql://ws-postgres:5432/ws
JDBC_USERNAME=ws
JDBC_PASSWORD=ws123

docker rm -f ws-sonar

docker run -d --name ws-sonar -p 9000:9000 \
    --link=ws-postgres:ws-postgres \
    -v ${FOLDER}/sonarqube/conf:/opt/sonarqube/conf \
    -v ${FOLDER}/sonarqube/data:/opt/sonarqube/data \
    -v ${FOLDER}/sonarqube/logs:/opt/sonarqube/logs \
    -v ${FOLDER}/sonarqube/extensions:/opt/sonarqube/extensions \
    -e sonar.jdbc.username=${JDBC_USERNAME} \
    -e sonar.jdbc.password=${JDBC_PASSWORD} \
    -e sonar.jdbc.url=${JDBC_URL} \
    sonarqube
