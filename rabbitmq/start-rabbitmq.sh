#!/usr/bin/env bash
# Help available at https://hub.docker.com/_/rabbitmq

SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

docker rm -f ws-rabbit

docker run -d \
  -p 15672:15672 \
  -e RABBITMQ_DEFAULT_USER=admin \
  -e RABBITMQ_DEFAULT_PASS=password \
  -e RABBITMQ_DEFAULT_VHOST=workshop \
  -v ${FOLDER}/rabbit:/var/lib/rabbitmq/mnesia/rabbit@ws-rabbit \
  --hostname ws-rabbit \
  --name ws-rabbit \
  rabbitmq:3-management
