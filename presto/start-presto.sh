#!/usr/bin/env bash
SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

${FOLDER}/../mongo/start-mongo.sh
${FOLDER}/../postgres/start-postgres.sh

cd ${FOLDER}

docker build -t ws-presto:latest -f Dockerfile .

docker rm -f ws-presto
rm -rf ${FOLDER}/logs
mkdir ${FOLDER}/data
chmod 777 -R ${FOLDER}/data

docker run -p 8080:8080 --name ws-presto -v ${FOLDER}/data:/data/presto ws-presto
