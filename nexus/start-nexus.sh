#!/usr/bin/env bash

SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

mkdir -p ${FOLDER}/data/log
mkdir -p ${FOLDER}/data/nexus3

chmod 777 -R ${FOLDER}/data

#docker rm -f ws-nexus

docker run -d -p 8081:8081 --name ws-nexus \
    -v ${FOLDER}/data/nexus3:/nexus-data \
    sonatype/nexus3
