#!/usr/bin/env bash

SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

docker rm -f ws-mssql
docker run --name ws-mssql \
    -e 'ACCEPT_EULA=Y' \
    -e 'SA_PASSWORD=Mssql@123' \
    -v ${FOLDER}/scripts:/scripts \
    -v ${FOLDER}/data:/var/opt/mssql \
    -p 1433:1433 \
    -p 1434:1434 \
    mcr.microsoft.com/mssql/server:2017-CU8-ubuntu
