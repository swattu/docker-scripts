#!/usr/bin/env bash
SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

docker rm -f ws-logstash

docker build -t ws-logstash:latest -f Dockerfile .

docker run --name ws-logstash \
    -it \
    -v ${FOLDER}/logstash/config:/usr/share/logstash/config \
    -v ${FOLDER}/logstash/data:/usr/share/logstash/data \
    -v ${FOLDER}/logstash/pipeline:/usr/share/logstash/pipeline \
    ws-logstash
