#!/usr/bin/env bash
SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

COUNT=`sysctl vm.max_map_count | cut -b20-`

if [[ ${COUNT} -ne "262144" ]]
then
    echo "Execute the following command"
    echo "sudo sysctl -w vm.max_map_count=262144"
    exit 1
fi

cd ${FOLDER}
export BASE_FOLDER=${FOLDER}

docker-compose down

#rm -rf ${FOLDER}/data*
mkdir ${FOLDER}/data01 ${FOLDER}/data02 ${FOLDER}/data03
chmod -R 777 ${FOLDER}/data01 ${FOLDER}/data02 ${FOLDER}/data03

docker-compose up
