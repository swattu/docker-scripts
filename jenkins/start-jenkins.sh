#!/usr/bin/env bash
SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

#${FOLDER}/../sonar/start-sonar.sh

mkdir ${FOLDER}/data
chmod 777 -R data

docker rm -f ws-jenkins

docker run --name ws-jenkins \
    --link=ws-sonar:ws-sonar \
    -p 8080:8080 -p 50000:50000 \
    -v ${FOLDER}/data:/var/jenkins_home \
    jenkins/jenkins:lts

