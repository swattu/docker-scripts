#!/usr/bin/env bash

SCRIPT=`realpath $0`
FOLDER=`dirname ${SCRIPT}`

docker rm -f ws-mysql
docker run -d --name ws-mysql \
    -e MYSQL_ROOT_PASSWORD=admin123 \
    -e MYSQL_DATABASE=ws \
    -e MYSQL_USER=ws \
    -e MYSQL_PASSWORD=ws123 \
    -p 3306:3306 \
    mysql:8.0
